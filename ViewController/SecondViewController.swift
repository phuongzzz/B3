//
//  SecondViewController.swift
//  ViewController
//
//  Created by phuongzzz on 7/5/18.
//  Copyright © 2018 phuongzzz. All rights reserved.
//

import UIKit

class SecondViewController: UIViewController {

    @IBOutlet weak var infoField: UITextField!
    
    @IBAction func sendData(_ sender: UIButton) {
        NotificationCenter.default.post(name: Notification.Name(rawValue: "myNotification"),
                                        object: nil,
                                        userInfo: ["message": self.infoField.text ?? ""])
        let firstViewController = self.storyboard?.instantiateViewController(withIdentifier: "firstVC") as! ViewController
        firstViewController.modalTransitionStyle = .flipHorizontal
        self.present(firstViewController, animated: true)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
}
