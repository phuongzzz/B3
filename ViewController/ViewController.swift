//
//  ViewController.swift
//  ViewController
//
//  Created by phuongzzz on 7/5/18.
//  Copyright © 2018 phuongzzz. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var infoLabel: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(changeInfoLabelText(notification:)),
                                               name: Notification.Name(rawValue: "myNotification"),
                                               object: nil)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

    @objc func changeInfoLabelText(notification: Notification) {
        if let message = notification.userInfo {
            if let msg = message["message"] as? String {
                self.infoLabel.text = msg
            }
        }
    }
}

